#!/usr/bin/env bash

currDir=`pwd`

for file in $(ls); do
  if [ -d $currDir/$file ]; then
    go test -v -count=1 $currDir/$file
  fi
done

