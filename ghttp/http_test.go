package ghttp

import (
	"strings"
	"testing"
)

func TestDoGet(t *testing.T) {
	url := "http://www.ftrako.com"
	res, err := DoGet(url)
	if err != nil {
		t.Error(err)
		return
	}
	if !strings.Contains(res, "总访问量") {
		t.Fail()
		return
	}
}

func TestDoPost(t *testing.T) {
	url := "http://www.ftrako.com/searchlist.html"
	res, err := DoPost(url, "")
	if err != nil {
		t.Error(err)
		return
	}
	if !strings.Contains(res, "ftrako") {
		t.Fail()
		return
	}
}
